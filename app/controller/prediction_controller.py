from flask import jsonify, Blueprint, request

from app.services.prediction_service.Prediction_service import Prediction_service

prediction_blueprint = Blueprint('prediction_blueprint', __name__)


@prediction_blueprint.route('', methods=['GET'])
# @token_required
def predict():
    product = {
        'category': request.args['category'],
        'product': request.args['product']
    }
    return jsonify(Prediction_service.predict(product))


@prediction_blueprint.route('/gen_model', methods=['POST'])
def price_predict_gen_model():
    return jsonify(Prediction_service.generateModel())
