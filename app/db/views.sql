CREATE VIEW product_bought AS
SELECT cart.user_id as user_id,
    product.product_id as product_id,
    product_category.category_name,
    product.product_name,
    sale.price as price,
    sale.quantity as quantity
FROM cart_sale
LEFT JOIN cart on cart.id = cart_sale.cart_id
LEFT JOIN sale on sale.sale_id = cart_sale.sale_id
LEFT JOIN product on product.product_id = sale.product_id
LEFT JOIN product_category on product_category.category_id = product.product_category_id;

select * from product_bought where RAND() < 0.01;
select user_id, produc_id, product_name, count(product_name) from product_bought where user_id = 51 OR user_id = 52 GROUP BY user_id, produc_id, product_name;

SELECT * FROM product_bought SAMPLE(10) WHERE ROWNUM <= 20;

CREATE TABLE fruit_vegetable (
	category VARCHAR(50),
	item VARCHAR(50),
	variety VARCHAR(100),
	date_rec DATE,
	price NUMBER,
	unit VARCHAR(5)
)