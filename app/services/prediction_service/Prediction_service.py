from app.db.Db import Db
from app.tools.Utils import Utils
import pandas as pd
import pickle
from datetime import datetime
from dateutil.relativedelta import relativedelta
from app.tools.Http_res import Http_res

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer  # For missing values
from sklearn.preprocessing import OneHotEncoder  # For categorical columns
from app.definitions import ROOT_PATH


class Prediction_service:
    def __init__(self):
        pass

    @staticmethod
    def importData():
        # Importing data
        # data_path = ROOT_PATH + '/assets/fruitvegprices-2017_2022.csv'
        # return pd.read_csv(data_path)
         with Db.connect() as db:
            query = pd.read_sql("select * from fruit_vegetable", db)
            df = pd.DataFrame(query)
            df.columns=df.columns.str.lower()
            df = df.rename(columns = {'date_rec':'date'})
            df['date'] = df['date'].dt.strftime('%Y-%m-%d')
            return df

    @staticmethod
    def data_preprocessing(product_data):
        # filtering product data row  by selecting only fruit and vegetable's category and kg unit
        product_data = product_data.loc[(product_data.category.isin(
            ('fruit', 'vegetable')) & (product_data.unit == 'kg'))]

        # product_data.date = splited_data[[0, 1]].apply("-".join, axis=1)

        # Price from £ to Ar
        # product_data[['price']] = product_data[['price']] * 4000
        # product_data['price'] = [price * 4000 for price in product_data['price']]
        for ind in product_data.index:
            product_data.loc[ind,
                             'price'] = product_data.loc[ind, 'price'] * 4000
        # rename item column to product
        product_data = product_data.rename({"item": "product"}, axis=1)

        return product_data

    @staticmethod
    def groupByColumns(product_data):
        # Split date into year, month column
        splited_data = product_data['date'].str.split('-', expand=True)[[0, 1]]
        product_data[['year', 'month']] = splited_data.loc[:].astype(int)

        # Group by some columns and get minimum price
        product_data = product_data.groupby(
            ['category', 'product', 'year', 'month', 'unit']).price.min().reset_index()

        return product_data

    @staticmethod
    def generateModel():
        try:
            product_data = Prediction_service.importData()
            product_data = Prediction_service.data_preprocessing(product_data)
            product_data = Prediction_service.groupByColumns(product_data)

            y = product_data.price
            X = product_data.drop(['price'], axis=1)
            X_train, X_valid, y_train, y_valid = train_test_split(
                X, y, train_size=0.8, test_size=0.2, random_state=20)

            categorical_cols = [
                cname for cname in X.columns if X[cname].dtype == "object"]

            # Select numerical columns
            numerical_cols = [
                cname for cname in X.columns if X[cname].dtype in ['int64', 'float64']]

            # Preprocessing for numerical data
            numerical_transformer = SimpleImputer(strategy='constant')

            # Preprocessing for categorical data
            categorical_transformer = Pipeline(steps=[
                ('onehot', OneHotEncoder(handle_unknown='ignore'))
            ])

            # Bundle preprocessing for numerical and categorical data
            preprocessor = ColumnTransformer(
                transformers=[
                    ('num', numerical_transformer, numerical_cols),
                    ('cat', categorical_transformer, categorical_cols)
                ])

            model = RandomForestRegressor(n_estimators=100, random_state=0)

            # Bundle preprocessing and modeling code in a pipeline
            pipeline = Pipeline(
                steps=[('preprocessor', preprocessor), ('model', model)])

            # Preprocessing of training data, fit model
            pipeline.fit(X_train, y_train)

            pickle.dump(pipeline, open(
                ROOT_PATH+'/services/prediction_service/price_prediction_model.pkl', 'wb'))
            return Http_res.send(200)
        except Exception as e:
            print("-----", e, "-----", sep="\n\n")
            return Http_res.send(400, err={"msg": 'an error occured'})

    @staticmethod
    def predict(product):
        # init number of month to predict
        month_pred = 6
        # Get today's date and replace day to 1 to avoid unexpected behavior such as 2020/01/29 + 1 month = ?
        date_pred = datetime.today().replace(day=1) + relativedelta(months=1)
        features = []
        # Get current folder path and import prediction model using pickle
        pickle_model = pickle.load(
            open(ROOT_PATH + '/services/prediction_service/price_prediction_model.pkl', 'rb'))
        for i in range(month_pred):
            features.append([product['category'], product['product'],
                            date_pred.year, date_pred.month, 'kg'])
            date_pred += relativedelta(months=1)

        features_df = pd.DataFrame(
            features, columns=['category', 'product', 'year', 'month', 'unit'])
        try:
            predictions = pickle_model.predict(features_df)
            features_df['pred'] = predictions.tolist()
            return Http_res.send(200, features_df.to_dict('records'))
        except Exception as e:
            print("-----", e, "-----", sep="\n\n")
            return Http_res.send(400, err={"msg": 'an error occured'})

    @staticmethod
    def translate(product_data):
        pass
