from functools import wraps
import jwt
from flask import request, current_app


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if "Authorization" in request.headers:
            token = request.headers["Authorization"].split(" ")[1]

        if not token:
            return {
                "status": "401",
                "data": None,
                "msg": "Authentication Token is missing",
                "error": "Unautorized"
            }
        try:
            jwt.decode(
                token, current_app.config["SECRET_KEY"], algorithms=["HS256"])
            # data = jwt.decode(token, "secret", algorithms=["HS256"])
            #  if current_user is None:
            #     return {
            #     "message": "Invalid Authentication token!",
            #     "data": None,
            #     "error": "Unauthorized"
            # }
        except jwt.ExpiredSignatureError:
            return {
                "status": "400",
                "data": None,
                "msg": "Authentication Token expired",
                "error": "Unautorized"
            }
        except Exception as e:
            return {
                "status": "400",
                "data": None,
                "msg": "Something went wrong",
                "error": str(e)
            }
        return f()
    return decorated
