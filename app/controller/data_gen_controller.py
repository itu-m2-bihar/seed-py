from flask import jsonify, Blueprint

from app.db.Data_gen import Data_gen

data_gen_blueprint = Blueprint('data_gen_blueprint', __name__)


@data_gen_blueprint.route('/categ', methods=['POST'])
def insert_categ():
    return jsonify(Data_gen.insertCategoryProduct())


@data_gen_blueprint.route('/product', methods=['POST'])
def insert_product():
    return jsonify(Data_gen.insertProduct())


@data_gen_blueprint.route('/sales', methods=['POST'])
def insert_sales():
    return jsonify(Data_gen.insertSales())


@data_gen_blueprint.route('/carts', methods=['POST'])
def insert_carts():
    return jsonify(Data_gen.insertCarts())


@data_gen_blueprint.route('/insertCartsSales', methods=['POST'])
def insert_carts_sales():
    return jsonify(Data_gen.insertCartsSales())


@data_gen_blueprint.route('/insertMvntSale', methods=['POST'])
def insert_mvnt_sale():
    return jsonify(Data_gen.insertMvntSale())


@data_gen_blueprint.route('/test', methods=['POST'])
def test():
    return jsonify({'msg': "hello it's working"})
