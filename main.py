import os
from flask import Flask, request, jsonify
# import py_eureka_client.eureka_client as eureka_client
# import asyncio
# import atexit
from app.controller.auth_middleware import token_required
from app.services.prediction_service.Prediction_service import Prediction_service
from app.db.Data_gen import Data_gen
from flask_cors import CORS
# from dotenv import load_dotenv
from app.controller.data_gen_controller import data_gen_blueprint
from app.controller.prediction_controller import prediction_blueprint
from app.controller.recommendation_controller import recommendation_blueprint

# load_dotenv()

# eureka_client.init(eureka_server="http://localhost:8761/eureka",
#                    eureka_context="/eureka/apps/",
#                    app_name="python-service",
#                    instance_host="localhost",
#                    instance_ip=host,
#                    instance_port=app.config['PORT'])

app = Flask(__name__)

# app.config['BASE_URL'] = os.environ.get('BASE_URL') or '/api/python'
# app.config['PORT'] = os.environ.get('PORT') or 5000
# base_url = "/api/python"
# host = "172.0.0.1"

# import secret key from .env
# SECRET_KEY = os.environ.get('SECRET_KEY') or 'this is a secret'
# app.config['SECRET_KEY'] = SECRET_KEY

cors = CORS(app, resources={"*": {"origins": "*"}})
# pickle_model = pickle.load(open('./model.pkl', 'rb'))


@app.route('/')
def index():
    return 'Hello python api'


app.register_blueprint(
    data_gen_blueprint, url_prefix='/data_gen')

app.register_blueprint(
    prediction_blueprint, url_prefix='/price/predict')

app.register_blueprint(
    recommendation_blueprint, url_prefix='/recommendation')


# def OnExitApp():
#     asyncio.run(eureka_client.get_client().stop())
#     print("Server shutown")

# if os.environ['FLASK_ENV'] == 'development':
#     atexit.register(OnExitApp)

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
