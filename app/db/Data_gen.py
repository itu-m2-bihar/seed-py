from datetime import datetime
import random
from app.db.Db import Db

from app.services.prediction_service.Prediction_service import Prediction_service
from app.tools.Http_res import Http_res
from app.definitions import ROOT_PATH
from datetime import datetime


class Data_gen:

    def __init__(self):
        pass

    # @staticmethod
    # def connection_params():
    #     return {
    #         'host': "109.70.148.50",
    #         'user': "dadamang_xeption",
    #         'password': "Xeption<3",
    #         'database': "dadamang_bihar",
    #         # 'host': "localhost",
    #         # 'user': "root",
    #         # 'password': "root",
    #         # 'database': "bihar_tpt",
    #     }

    @staticmethod
    def insertCategoryProduct():
        try:
            product_data = Prediction_service.data_preprocessing(
                Prediction_service.importData())

            with Db.connect() as db:
                with db.cursor() as c:
                    for categ in product_data['category'].unique():
                        request = 'insert into product_category(category_id, category_name, version) values (PRODUCT_CATEGORY_SEQ.nextval, :1,0)'
                        params = [categ]
                        c.execute(request, params)
                        db.commit()
                    c.close()
            return Http_res.send(200)
        except Exception as e:
            if (c is not None):
                c.close()
            if (db is not None):
                db.close()
            print("-----", e, "-----", sep="\n\n")
            return Http_res.send(400, err={"msg": 'an error occured'})

    @staticmethod
    def insertProduct():
        try:
            product_data = Prediction_service.data_preprocessing(
                Prediction_service.importData())
            product_data = product_data.groupby(['category', 'product'], as_index=False).aggregate({
                'category': 'first', 'product': 'first'})
            with Db.connect() as db:
                with db.cursor() as c:
                    c.execute("SELECT * FROM product_category")
                    categories = c.fetchall()
                    tmp_array = []
                    def insertProductData(product, product_category):
                        for category in categories:
                            if (category[2] == product_category):
                                params = [product, category[0],f'{product}.jpg']
                                tmp_array.append(params)
                                # c.execute(request, params)
                    [insertProductData(product, product_category)
                     for product, product_category in product_data[['product', 'category']].itertuples(index=False)]
                    request = 'insert into product(product_id, product_name, product_category_id, image, version) values (PRODUCT_SEQ.nextval,:1,:2,:3,0)'
                    c.executemany(request, tmp_array)
                    db.commit()
            return Http_res.send(200)
        except Exception as e:
            if (c is not None):
                c.close()
            if (db is not None):
                db.close()
            print("-----", e, "-----", sep="\n\n")
            return Http_res.send(400, err={"msg": 'an error occured'})

    @staticmethod
    def insertSales():
        try:
            sale_data = Prediction_service.data_preprocessing(
                Prediction_service.importData())
            with Db.connect() as db:
                with db.cursor() as c:
                    c.execute("SELECT * FROM product")
                    products = c.fetchall()
                    column_names = [str(row[0]).lower() for row in c.description]
                    tmp_array = []
                    def insertSaleData(sale_product, sale_price, sale_date):
                        for product in products:
                            if (product[column_names.index("product_name")].lower() == sale_product.lower()):
                                tmp_array.append([product[column_names.index("product_id")],sale_price, random.randrange(1, 5, 1),sale_date,sale_date, 2,0])
                    [insertSaleData(sale_product, sale_price, sale_date)
                     for sale_product, sale_price, sale_date in sale_data[['product', 'price', 'date']].itertuples(index=False)]
                    request = "insert into sale(sale_id, product_id, price, quantity,date_created, last_updated, user_id, sale_state, version) values (SALE_SEQ.nextval,:1,:2,:3,TO_DATE(:4,'YYYY-MM-DD'),TO_DATE(:5,'YYYY-MM-DD'),:6,:7,0)"
                    c.executemany(request, tmp_array)
                    db.commit()
            return Http_res.send(200)
        except Exception as e:
            e.with_traceback()
            print("-----", e, "-----", sep="\n\n")
            return Http_res.send(400, err={"msg": 'an error occured'})
        
    @staticmethod
    def insertCarts():
        try:
            with Db.connect() as db:
                with db.cursor() as c:
                    c.execute("SELECT * from sale")
                    sales = c.fetchall()
                    tmp_array = []
                    now = datetime.today()
                    column_names = [str(row[0]).lower() for row in c.description]
                    cart_sale = []
                    user_id = 1
                    cart_id = 1
                    user_dict = {}
                    count_product = {}
                    t = 0
                    while (len(sales)!=0) & (len(tmp_array)<600) == True:
                        insert = False
                        if user_id == 51:
                            user_id = 1
                        # register user and list of product
                        # user should have max 4 kind of product
                        if(str(user_id) not in user_dict):
                            user_dict.setdefault(str(user_id), [])
                        sale_index = random.randint(0,len(sales)-1)
                        product_id = sales[sale_index][column_names.index('product_id')]
                        if(str(product_id) not in count_product):
                            count_product.setdefault(str(product_id), 0)
                        if(count_product.get(str(product_id)) > 10):
                            sales.pop(sale_index)
                            continue
                        # check if length of user unique product's list is already equal to 4
                        if product_id in user_dict[str(user_id)]:
                            insert = True
                        else:
                            if len(user_dict[str(user_id)]) < 4:
                                user_dict[str(user_id)].append(product_id)
                                insert = True
                        if insert:
                            count_product[str(product_id)]+=1
                            tmp_array.append([cart_id, user_id, now, now, -100,0])
                            # register cart with sale
                            sale_id = sales[sale_index][column_names.index('sale_id')]
                            cart_sale.append([cart_id, sale_id])
                            # print(cart_id, sale_id, user_id, product_id, sep=" ")
                            cart_id+=1
                        user_id+=1
                        sales.pop(sale_index)
                        
                    request="insert into cart(id, user_id, date_created, last_updated, cart_state, version) values (:1,:2,TO_DATE(:3,'YYYY-MM-DD'),TO_DATE(:4,'YYYY-MM-DD'),:5,:6)"
                    c.executemany(request, tmp_array)
                    print('cart data inserted')
                    request="insert into cart_sale(cart_id, sale_id) values (:1,:2)"
                    c.executemany(request, cart_sale)
                    print('cart_sale data inserted')
                    db.commit()
            return Http_res.send(200)
        except Exception as e:
            print("-----", e, "-----", sep="\n\n")
            return Http_res.send(400, err={"msg": 'an error occured'})

    # @staticmethod
    # def insertMvntSale():
    #     try:
    #         with Db.connect() as db:
    #             with db.cursor() as c:
    #                 f = open(
    #                     '/media/kweiz4r/68EC5C4AEC5C1522/study/m2/TPT/project-seed/mvnt_sale.sql', 'a')
    #                 c.execute(
    #                     "SELECT id, user_id from cart WHERE cart_state = 1")
    #                 carts = c.fetchall()
    #                 for cart in carts:
    #                     f.write(f"""insert into mvnt_sale(cart_id, user_id, price,mvnt_type)
    #                         values ({cart[0]},{cart[1]},100, 1);\n""")
    #                 f.close()
    #                 c.close()
    #                 db.close()
    #         return Http_res.send(200)
    #     except Exception as e:
    #         if (f is not None):
    #             f.close()
    #         if (c is not None):
    #             c.close()
    #         if (db is not None):
    #             db.close()
    #         print("-----", e, "-----", sep="\n\n")
    #         return Http_res.send(400, err={"msg": 'an error occured'})
