# syntax=docker/dockerfile:1

FROM python:3.8

WORKDIR /seed-py-docker

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

ENV LD_LIBRARY_PATH=/opt/oracle/instantclient_21_7

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV FLASK_APP=main.py

ADD instantclient-basic-linux.x64-21.7.0.0.0dbru.zip ./
RUN mkdir -p /opt/oracle && \
    unzip instantclient-basic-linux.x64-21.7.0.0.0dbru.zip && \
    mv instantclient_21_7 /opt/oracle && \
    rm instantclient-basic-linux.x64-21.7.0.0.0dbru.zip

RUN apt-get update && apt-get install libaio1

COPY . /seed-py-docker

CMD gunicorn main:app