from cmath import log
from app.db.Db import Db

from app.tools.Http_res import Http_res
from app.definitions import ROOT_PATH
import pandas as pd

import turicreate as tc

from sklearn.model_selection import train_test_split


class Recommendation_service:
    @staticmethod
    def exportData():
        try:
            df = Recommendation_service.get_pd_purchase_data()
            df.to_csv(ROOT_PATH + '/assets/purchase_data.csv')
            return Http_res.send(200)
        except Exception as e:
            print("-----", e, "-----", sep="\n\n")
            return Http_res.send(400, err={"msg": 'an error occured'})

    @staticmethod
    def exportProductData():
        try:
            df = Recommendation_service.get_pd_product_data()
            df.to_csv(ROOT_PATH + '/assets/product_data.csv')
            return Http_res.send(200)
        except Exception as e:
            print("-----", e, "-----", sep="\n\n")
            return Http_res.send(400, err={"msg": 'an error occured'})

    @staticmethod
    def exportUserData():
        try:
            df = Recommendation_service.get_pd_user_data()
            df.to_csv(ROOT_PATH + '/assets/user_data.csv')
            return Http_res.send(200)
        except Exception as e:
            print("-----", e, "-----", sep="\n\n")
            return Http_res.send(400, err={"msg": 'an error occured'})

    @staticmethod
    def get_pd_purchase_data():
        with Db.connect() as db:
            query = pd.read_sql("select * from product_bought", db)
            df = pd.DataFrame(query)
            df.columns= df.columns.str.lower()
        return df

    @staticmethod
    def get_pd_product_data():
        with Db.connect() as db:
            query = pd.read_sql("select * from product", db)
            df = pd.DataFrame(query)
            df.columns= df.columns.str.lower()
            df = df.drop(df.columns[[0]], axis=1)
            df = df.drop('version', axis=1)
        return df

    @staticmethod
    def get_pd_user_data():
        with Db.connect() as db:
            query = pd.read_sql("select * from user_agro", db)
            df = pd.DataFrame(query)
            df.columns= df.columns.str.lower()
            df = df.drop(df.columns[[0]], axis=1)
            df = df.drop('version', axis=1)
        return df

    @staticmethod
    def generate_popularity_based_model():
        try:
            # # product_data = Recommendation_service.get_pd_product_data()
            # # user_data = Recommendation_service.get_pd_user_data()
            purchase_data = Recommendation_service.get_pd_purchase_data()
            purchase_data = Recommendation_service.data_preprocessing(
                purchase_data)
            purchase_matrix = pd.pivot_table(
                purchase_data, values='purchase_count', index='user_id', columns='product_id')
            # norm_data = Recommendation_service.normalize_data(purchase_matrix)
            # users_to_recommend = list(user_data['id'])
            # n_rec = 10 # number of items to recommend
            # n_display = 30 # to display the first few rows in an output dataset
            model = tc.popularity_recommender.create(
                tc.SFrame(purchase_data), user_id='user_id', item_id='product_id', target='purchase_count')
            model.save(
                ROOT_PATH+'/services/recommendation_service/rec_pop_model.tc')
            return Http_res.send(200, data={"msg": "Succesfully generated popularity based model"})
        except Exception as e:
            e.with_traceback()
            print("-----", e, "-----", sep="\n\n")
            return Http_res.send(400, err={"msg": 'an error occured'})

    # Colaborative Filtering based model
    @staticmethod
    def generate_cf_based_model():
        try:
            # product_data = Recommendation_service.get_pd_product_data()
            # user_data = Recommendation_service.get_pd_user_data()
            purchase_data = Recommendation_service.get_pd_purchase_data()
            purchase_data = Recommendation_service.data_preprocessing(
                purchase_data)
            purchase_matrix = pd.pivot_table(
                purchase_data, values='purchase_count', index='user_id', columns='product_id')
            # norm_data = Recommendation_service.normalize_data(purchase_matrix)
            model = tc.item_similarity_recommender.create(tc.SFrame(
                purchase_data), user_id='user_id', item_id='product_id', target='purchase_count', similarity_type='cosine')
            model.save(
                ROOT_PATH+'/services/recommendation_service/rec_cf_model.tc')
            return Http_res.send(200, data={"msg": "Succesfully generated cf based model"})
        except Exception as e:
            print("-----", e, "-----", sep="\n\n")
            return Http_res.send(400, err={"msg": 'an error occured'})

    @staticmethod
    def data_preprocessing(purchase_data):
        purchase_data = purchase_data.drop('price', axis=1)
        purchase_data = purchase_data.dropna(axis=0)
        purchase_data['user_id'] = purchase_data.loc[:, 'user_id'].astype(int)
        purchase_data['product_id'] = purchase_data.loc[:,
                                                        'product_id'].astype(int)
        purchase_data = purchase_data.loc[:, ['user_id', 'product_id']]
        purchase_data = purchase_data.groupby(
            ['user_id', 'product_id']).agg({'product_id': 'count'}).rename(columns={'product_id': 'purchase_count'}).reset_index()
        return purchase_data

    @staticmethod
    def normalize_data(purchase_matrix):
        purchase_matrix_norm = (purchase_matrix - purchase_matrix.min()) / \
            (purchase_matrix.max() - purchase_matrix.min())
        d = purchase_matrix_norm.reset_index()
        d.index.names = ['purchase_freq']
        return pd.melt(d, id_vars=['user_id'], value_name='purchase_freq').dropna()

    @staticmethod
    def recommend_with_pop_based_model():
        model = tc.load_model(
            ROOT_PATH+'/services/recommendation_service/rec_pop_model.tc')
        recom = model.recommend(users=[10], k=10).to_dataframe()
        recom = recom.drop(['score', 'user_id'], axis=1)
        return recom

    @staticmethod
    def recommend_with_cf_based_model(user_id):
        model = tc.load_model(
            ROOT_PATH+'/services/recommendation_service/rec_cf_model.tc')
        recom = model.recommend(users=[user_id], k=10).to_dataframe()
        recom = recom.drop(['score', 'user_id'], axis=1)
        return recom

    @staticmethod
    def recommend_product(user_id, connection_state=0):
        try:
            products = []
            recom = None
            if connection_state == 0:
                print(f"------------- is pop")
                Recommendation_service.generate_popularity_based_model()
                recom = Recommendation_service.recommend_with_pop_based_model()
            else:
                if Recommendation_service.check_user_product_count(user_id) > 0:
                    print(f"------------- is cf - user_id:{user_id}")
                    Recommendation_service.generate_cf_based_model()
                    recom = Recommendation_service.recommend_with_cf_based_model(user_id)
                else:
                    print("------------- is pop")
                    Recommendation_service.generate_popularity_based_model()
                    recom = Recommendation_service.recommend_with_pop_based_model()
            products = Recommendation_service.get_product_recommended(recom.loc[:3,'product_id'].values.tolist())
            return Http_res.send(200, data=products)
        except Exception as e:
            e.with_traceback()
            return Http_res.send(400, err={"msg": 'an error occured'})
        
    @staticmethod
    def check_user_product_count(user_id):
        try:
            count = 0
            with Db.connect() as db:
                with db.cursor() as c:
                    request="select user_id, count(product_name) AS product_count from product_bought where user_id = :1 GROUP BY user_id"
                    c.execute(request, [user_id])
                    product = c.fetchone()
                    count = product[1]
            return count
        except Exception as e:
            raise Exception("An error occured while checking user product count")
        
    @staticmethod
    def get_product_recommended(product_ids):
        try:
            with Db.connect() as db:
                with db.cursor() as c:
                    id_str_list = ''
                    for i in range(len(product_ids)):
                        id_str_list += f':{i + 1},'
                    id_str_list = id_str_list[:-1]
                    request=f"select * from product where product_id in ({id_str_list})"
                    c.execute(request, product_ids)
                    products = c.fetchall()
                    products = [{'product_id':product[0], 'product_name':product[4], 'image':product[3]} for product in products]
                    return products
        except Exception as e:
            e.with_traceback()
            raise Exception("An error occured while getting product recommended")