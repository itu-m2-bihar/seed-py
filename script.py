import sys
import subprocess

# implement pip as a subprocess:
subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'jwt'])
subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'scikit-learn'])
subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'pandas'])
subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'python-dotenv'])
subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'flask'])
subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'flask-cors'])
subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'turicreate'])
subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'oracledb'])