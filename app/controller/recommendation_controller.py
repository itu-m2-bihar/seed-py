from flask import jsonify, Blueprint, request

from app.services.recommendation_service.recommendation import Recommendation_service
from app.tools.Http_res import Http_res

recommendation_blueprint = Blueprint('recommendation_blueprint', __name__)


@recommendation_blueprint.route('/exportData', methods=['POST'])
def export_data():
    return jsonify(Recommendation_service.exportData())


@recommendation_blueprint.route('/export/productData', methods=['POST'])
def export_product_data():
    return jsonify(Recommendation_service.exportProductData())


@recommendation_blueprint.route('/export/userData', methods=['POST'])
def export_user_data():
    return jsonify(Recommendation_service.exportUserData())


@recommendation_blueprint.route('/generate/popularityModel', methods=['POST'])
def generate_popularity_model():
    return jsonify(Recommendation_service.generate_popularity_based_model())


@recommendation_blueprint.route('/generate/cfModel', methods=['POST'])
def generate_cf_model():
    return jsonify(Recommendation_service.generate_cf_based_model())


@recommendation_blueprint.route('/recommend/byPopularity', methods=['GET'])
def recommend_with_pop_based_model():
    return jsonify(Recommendation_service.recommend_with_pop_based_model())


@recommendation_blueprint.route('/recommend/byCf', methods=['GET'])
def recommend_with_cf_based_model():
    user_id = request.args['userId']
    return jsonify(Recommendation_service.recommend_with_cf_based_model(user_id))

@recommendation_blueprint.route('/recommend/product', methods=['GET'])
def recommend_product():
    user_id = request.args.get('userId', None)
    state = request.args['state']
    if(state.isdigit()):
        state = int(state)
        return jsonify(Recommendation_service.recommend_product(user_id, state))
    else:
        return jsonify(Http_res.send(400, err={"msg": 'an error occured while getting recommendation product'}))